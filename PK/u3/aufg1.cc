#include <iostream>
#include <cmath>


int main (int argc, char** argv){
    
    
    std::cout<< "Die quadratische Gleichung hat die form ax^2+bx+c=0" <<std::endl;
    double v;
    
    
    std::cout << "Gebe a an"<< std::endl;
    std::cout << "a= " << std::flush;
    std::cin >> v;
    
    double x;
    std::cout << "Gebe b an"<< std::endl;
    std::cout << "b= " << std::flush;
    std::cin >> x;
    
    double y;
    std::cout << "Gebe c an"<< std::endl;
    std::cout << "c= " << std::flush;
    std::cin >> y;
    
    if ( x*x-4*v*y >= 0){
    double l1;
    double l2;
    double w= std::sqrt(x*x-4*v*y);
    
  
    l1= -x+w;
    l1= l1/(2*v);
    l2= -x-w;
    l2= l2/(2*v);
    if ( l1 == l2){
        std::cout << " Keine eindeutige Lösung, gibt nur eine Lösung mit l=" << l1 << std::endl;
    }
    else {
    std::cout << "Die erste Lösung ist " << l1 << " Die zweite Lösung ist " << l2 << std::endl;
    }
    
    }
    else {
        std::cout << "keine Lösung möglich" << std::endl;
    }
    

    
}

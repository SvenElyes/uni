#include "io.hh"
#include <iostream>
#include <vector>
#include "statistics.hh"

int main (int argc, char** argv){
	std::vector <double> v1 = normal_distribution(random_seed(),10,5,3);
	for (double & value  : v1){
		std::cout << value << std::endl;
	}
	std::cout << "durschnitt:" << mean(v1) << std::endl;
 std::cout << "Standarddeviation:" << standard_deviation(v1) << std::endl;
	
}

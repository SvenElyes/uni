#include "io.hh"
#include <iostream>
#include <vector>
#include "statistics.hh"

int main (int argc, char** argv){
	std::vector <double> v1 = uniform_distribution(random_seed(),10,4,30);
	for (double & value: v1){
		std::cout<< value << std::endl;
	}
	std::cout << "Mittelwert:" << standard_deviation(v1) << std::endl;
}

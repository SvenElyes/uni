#include <iostream>
#include <map>
#include <string>
#include "sanitizeword.hh"

std::map <std::string , int> get_frequencies(){
	std::map <std::string,int > rueckg;
	int  laenge;
	
	while(true){
		
		 std::string  c;
		
		std::cin >> c;
		
		
		if (not std::cin){
			
			break;
		}
		
		
		c= sanitize_word(c);	
		if ( c.size() != 0){
		rueckg[c]= rueckg[c]+1;
		}
		
		
		}
		return rueckg;
	}	
	


void print_frequencies(const std::map <std::string,int> & frequencies){
	for( const std::pair  <  std::string,  int > &   value : frequencies){
		std::cout << "Wort: " << value.first << " Frequency:" << value.second << std::endl;
		
	}
}

int main  ( int argc, char** argv){
	print_frequencies(get_frequencies());
}

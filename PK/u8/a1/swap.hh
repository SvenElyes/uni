//template.hh

#ifndef SWAP_HH
#define SWAP_HH
#include <iostream>
template <typename T>
void swap ( T & x, T &y) {
	T v1=x;
	T v2=y;
	x= v2;
	y= v1;
	
}
	
#endif 

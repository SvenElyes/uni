#ifndef STATISTICS_HH
#define STATISTICS_HH

#include <vector>
#include <algorithm>
#include <cmath>

template <typename T >

double mean(const T & v){
	double sum = 0;
	double size = v.size();
    for (auto e : v) {
		
    sum += e;
}
double b = sum / size;
    return b;
}
	
template <typename T >

double median ( const T & v ){
  //auto v2(v); frag nach wie es mit for geht 
  auto b = v;
  std::vector <typename T::value_type> v2;
  
  for ( auto g : v){
	  v2.push_back(g);
  }
  std::sort(v2.begin(),v2.end());

  if (v.size() % 2 == 0)
    {
      return 0.5 * (v2[v.size()/2 - 1] + v2[v.size()/2]);
    }
  else
    {
      return v2[(v.size()+1)/2 - 1];
    }
	
}


template <typename S>
double moment(const S & v, int k){
 double sum = 0;
  for (auto e : v)
    sum += std::pow(e,k);

  return sum / v.size();
}
template <typename H >

double std_dev(const H & v){
	
	double m = mean(v);
	double sum = 0;
	for (auto e : v)
    sum += (m - e)*(m - e);

  return std::sqrt(sum / v.size());
}

template <typename G>

void statistics(const G& v){
	std::cout << "mean: " << mean(v) << std::endl;
	std::cout << "median: " << median(v) << std::endl;
	std::cout << "second moment: " << moment(v,2) << std::endl;
	std::cout << "std_dev: " << std_dev(v) << std::endl;
}

#endif // STATISTICS_HH

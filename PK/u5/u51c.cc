#include <vector>
#include <iostream>
#include <utility>

void  vtausch ( std::vector <double> & v){
	// für gerade n bis zur hälfte
	if ( v.size() % 2 ==0){
	for ( int j=0; j<(v.size()/2); j++){
		
		std::swap ( v[j], v[v.size()-1-j]);
		//std::cout << v[j] << std::endl;
	}
  }
  // bei ungeraden n bleibt der 2n - +.5 wert erhalten
  //bei int wird nach dem . abgeschnibbelt
   else {
	   for (int j=0; j<(v.size()/2);j++){
	  std::swap ( v[j], v[v.size()-1-j]);
  }
  }
}

int main (int argc, char** argv){
	std::vector <double> vec;
	vtausch(vec);
	for ( double & gang : vec){
		std::cout << gang << std::endl;
	}
}

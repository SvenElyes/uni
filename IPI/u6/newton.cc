#include "fcpp.hh"
int globalschritte=0;
bool gut_genug( double xn, double a ) {
  return fabs( xn*xn - a ) <= 1e-15;
}

double wurzelIter( double xn, double a ) {
	globalschritte=globalschritte+1;
  return cond( gut_genug( xn, a ),
               xn,
               wurzelIter( 0.5*(xn + a/xn), a ) );
}
  
double wurzel( double a )
{
  return wurzelIter( 1.0, a );
}

int main( int argc, char *argv[] )
{

	double c;
	std::cin >> c;
	print(wurzel (c));
		std::cout << "globschr:" << globalschritte<<std::endl;
    //print( wurzel( readarg_double( argc, argv, 1 ) ) );
}

#include <iostream>
// funktion für outshuffle
void outshuffle ( int  deck[]){
	// teilen des decks in zwei teile 
	int kopie1 [26];
	int kopie2 [26];
	// weist der kopie1 die ersten 26 karten zu
	for (int j=0; j<26; j++){
		kopie1[j]= deck[j];
	}
	// weist der kopie2 die letzten 26 karten zu
	for (int g=0; g<26; g++){
		kopie2[g]= deck[g+26];
		
	}
	// jetzt deck  verändern 
	for ( int h=0; h <52; h++){
		if ( h % 2 == 0){
			deck[h]= kopie1[h/2];	
		}
		else if (h % 2 == 1) {
			//funktioniert da 9/2 = 4 ist da int absnibbelt
		deck[h]= kopie2[h/2];	
		}
	}

	
}
//inshuffle ähnlich wie outshuffle fast nur gespiegelte shuffle mechanism
void inshuffle ( int deck[]){
	int kopie1 [26];
	int kopie2 [26];
	for (int j=0; j<26; j++){
		kopie1[j]= deck[j];
	}
	for (int g=0; g<26; g++){
		kopie2[g]= deck[g+26];	
	}
	for ( int h=0; h <52; h++){
		if ( h % 2 == 0){
				deck[h]= kopie2[h/2];	
		}
		else if (h % 2 == 1) {
			//funktioniert da 9/2 = 4 ist da int absnibbelt
		
		deck[h]= kopie1[h/2];
		}
	}
	
}
//checkt das deck ob es in ursprungsform ist
bool deck_check (int deck [], int n){
	// erstellt eine versionskontrolle die aussieht wie das ungemischte deck
	 int verskontrolle [52];
	for (int j =0; j<n; j++){	
		verskontrolle[j]=j;
	}
	//erstellt die variable kontrolle und diese wird bei jedem richtigen wert inkremitiert. 
	int kontrolle =0;
	
	for (int h=0 ; h<n;h++){
		if ( deck[h] == verskontrolle[h]){
			kontrolle = kontrolle +1;
			
		}
	}
	// gibt true zurück wenn alle karten am richtigen platz sind
	if (kontrolle ==  n){
		return true;
	}
	else {
		return false;
	}
}

int main (int argc, char **argv){
	 int deck [52];
	 
	
	for (int j =0; j<52; j++){
		deck [j]=j;	
		
	}
	
	
	//ein outshuffle bzw ein inshuffle vor der schleife damit es funktioniert . Erster schritt wird ausgeladen
	// weclhes was man nicht benutzt einfach auskommentieren und das andere dastehen lassen 
	//vvvvvvvvvvvvvvvvv
	outshuffle(deck);
	//inshuffle (deck);
	int schritte=1;
	
while (deck_check(deck,52)== false){
	//inshuffle(deck);
	outshuffle(deck);
	schritte=schritte+1; 
	}
	//gibt die schritte aus
  std::cout << "Schritte für den Müll" << schritte << std::endl;
}

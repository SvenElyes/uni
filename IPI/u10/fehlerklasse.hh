#ifndef FEHLERKLASSE_HH
#define FEHLERKLASSE_HH
class Fehlerklasse {
public:
Fehlerklasse ( double a , double b) ;

Fehlerklasse operator + (Fehlerklasse arg1);
Fehlerklasse operator * (Fehlerklasse arg1);

double wert();
double absFehler ();
double relFehler() ;

private:

double _wert;
double _absFehler;
double _relFehler= _absFehler/_wert;

};

#endif // ITERATIONSRESULTS_HH

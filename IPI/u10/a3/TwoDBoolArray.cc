
public:
	TwoDBoolArray::TwoDBoolArray (int n =0, int m = 0);
	TwoDBoolArray::TwoDBoolArray( const TwoDBoolArray& other) ;
	TwoDBoolArray::~TwoDBoolArray();
	TwoDBoolArray& TwoDBoolArray::operator = ( const TwoDBoolArray& other);
	int rows();
	int cols();
	
	class RowProxy 
	{
		public :
		RowProxy ( bool * daten, int zeilenindex, int spaltenzahl);
		bool& operator [] ( int j);
		private: 
		bool * _daten;
		int zeilenindex;
		int spaltenzahl;
	};
	RowProxy operator [] (int i );
	private : 
	bool* daten;

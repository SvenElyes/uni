#include "fehlerklasse.hh"
#include <cmath>
Fehlerklasse::Fehlerklasse ( double a, double b ) : _wert(a), _absFehler(b) {}

double Fehlerklasse::wert(){
	return _wert;
}
	
double Fehlerklasse::absFehler (){
	return _absFehler;
}
double Fehlerklasse::relFehler(){
	return _relFehler;
}
Fehlerklasse Fehlerklasse::operator + (Fehlerklasse arg1){
	double summe;
	double summe2;
	summe= arg1.absFehler()*arg1.absFehler() + _absFehler*_absFehler ;
	summe = sqrt(summe);
	
	summe2= arg1.wert()+_wert;
	Fehlerklasse fk( summe2,summe);
	return fk;
	
	
}
Fehlerklasse Fehlerklasse::operator * (Fehlerklasse arg1){
	
	double p;
	double relativerf;
	double afehler;
	p= arg1.wert() * _wert;
	relativerf =(arg1.absFehler()/arg1.wert())*(arg1.absFehler()/arg1.wert())+ (_absFehler/_wert)*(_absFehler/_wert);
	relativerf= sqrt(relativerf);
	afehler = relativerf*p;
	Fehlerklasse fk ( p, afehler);
	return fk ;
	
}

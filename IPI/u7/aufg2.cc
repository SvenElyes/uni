
#include <iostream>
using namespace std;

#include "fcpp.hh"



const int ArrayLength = 10;
int Arr[ArrayLength];
int inx = 0;
int outx = 0;


int modulo(int a){                                      //modulofunkion, da Ring wie eine uhr "funktioniert" und alle Werte die größer als der Ring sind, werden "angepasst"
    int b = a % ArrayLength;
    return b;
}

void mInsert(int a) {
    if (modulo(inx) == modulo(outx) && Arr[modulo(inx)] != 0) {     //funktion für das Einschreiben
        print("Overwrite");                                     //verwriten falls ein alter Wert überschrieben wird
        outx = outx + 1;
    }
    Arr[modulo(inx)] = a;
    inx = inx + 1;
}

int mExtract() {                                    //Wert wird ausgelesen, bei Eingabe einer 0
    int extrct = Arr[modulo(outx)];
    Arr[modulo(outx)] = 0;
    outx = outx + 1;                                //out wandert eins weiter da ältester Wert weg ist
    return extrct;
}

void ArrInit() {
    for (int n = 0; n < ArrayLength; n++)
        Arr[n] = 0;                                 //zu beginn wird das Array nur mit 0 initialisiert
}

void ArrPrint() {
    print("---------");                               //Ausgabe des Rings, der Position von inx, outx
    print("Ring");
    for (int n = 0; n < ArrayLength; n++)
        print(Arr[n]);
    print("---------");
    print("outx:");
    print(modulo(outx));
    print("inx:");
    print(modulo(inx));
    print("---------");
}

int main(){
    int r;
    
    ArrInit();
    ArrPrint();
    cin >> r;
    while (r >= 0) {                                //Schleife für die Eingabe
        if (r == 0)
           print(mExtract());                       // falls r = 0 -> ausgelsen
        else
            mInsert(r);
        ArrPrint();
        cin >> r;
    }
}












